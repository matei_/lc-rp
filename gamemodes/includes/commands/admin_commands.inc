/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

/* --- admin commands --- */
CMD:createfaction(playerid, params[])
{
	new facName[42], facType, facMaxMembers, facIsAdmin, addFacQuery[256];

	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(MasterInfo[playerid][masterAdmin] < 5)
		return SendUnauthMessage(playerid, "Only admin 5+ can use this command.");

	if(sscanf(params, "iiis[42]", facType, facMaxMembers, facIsAdmin, facName))
		return SendUsageMessage(playerid, "/createfaction [type] [max members (max 128)] [admin (1 - yes / 0 - no)] [name]");
	if(strlen(facName) < 8 || strlen(facName) > 42)
		return SendErrorMessage(playerid, "The name of the organization must be between 8 and 42 characters.");
	if(facType < 1 || facType > 5)
		return SendErrorMessage(playerid, "The type of organization must be: 1 - Government, 2 - Law Enforcement, 3 - Medical, 4 - Legal, 5 - Illegal.");
	if(facMaxMembers < 12 || facMaxMembers > 128)
		return SendErrorMessage(playerid, "Organization minimum members must be 12 or maximum members must be 128.");
	if(facIsAdmin < 0 || facIsAdmin > 1)
		return SendErrorMessage(playerid, "The organization admin option must be 1 or 0.");

	mysql_format(g_SQL, addFacQuery, sizeof(addFacQuery), "INSERT INTO factions (factionType, factionName, factionMaxMembers, factionIsAdminCreated) VALUES ('%d', '%e', '%d', '%d')", facType, facName, facMaxMembers, facIsAdmin);
	new Cache: addFac = mysql_query(g_SQL, addFacQuery);
	new i = cache_insert_id();

	format(FactionInfo[i][factionName], 42, "%s", facName);
	FactionInfo[i][factionType] = facType;
	FactionInfo[i][factionMaxMembers] = facMaxMembers;


	FactionInfo[i][factionIsAdminCreated] = facIsAdmin;

	cache_delete(addFac);
	SendServerMessage(playerid, "This organization has been added with success in our database.");
	SendMessage(playerid, "[ID: %d | Name: %s | Type: %d | Max Members: %d | Admin created: %d]", i, facName, facType, facMaxMembers, facIsAdmin);
	return 1;
}
alias:createfaction("createfac", "createf");

CMD:createvehicle(playerid, params[])
{
	new vehModel, addVehQuery[256];

	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(MasterInfo[playerid][masterAdmin] < 5)
		return SendUnauthMessage(playerid, "Only admin 5+ can use this command.");

	if(sscanf(params, "i", vehModel))
		return SendUsageMessage(playerid, "/createvehicle [model id]");

	if(vehModel < 400 || vehModel > 611)
		return SendErrorMessage(playerid, "The vehicle model must be between 400 and 611.");

	new Float: playerPos[4];
	GetPlayerPos(playerid, playerPos[0], playerPos[1], playerPos[2]);
	GetPlayerFacingAngle(playerid, playerPos[3]);

	new vehID = CreateVehicle(vehModel, playerPos[0], playerPos[1], playerPos[2], playerPos[3], -1, -1, -1, 0);

	PutPlayerInVehicle(playerid, vehID, 0);
	LinkVehicleToInterior(vehID, GetPlayerInterior(playerid));

	mysql_format(g_SQL, addVehQuery, sizeof(addVehQuery), "INSERT INTO vehicles (VehicleModel, VehicleParkPosX, VehicleParkPosY, VehicleParkPosZ, VehicleParkPosA, VehicleParkInterior, VehicleParkWorld) VALUES ('%d','%f', '%f', '%f', '%f', '%d', '%d')", vehModel, playerPos[0], playerPos[1], playerPos[2], playerPos[3], GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));
	new Cache: addVeh = mysql_query(g_SQL, addVehQuery);
	new i = cache_insert_id();

	VehicleInfo[i][eVehicleModel] = vehModel;
	VehicleInfo[i][eVehicleParkPos][0] = playerPos[0];
	VehicleInfo[i][eVehicleParkPos][1] = playerPos[1];
	VehicleInfo[i][eVehicleParkPos][2] = playerPos[2];
	VehicleInfo[i][eVehicleParkPos][3] = playerPos[3];
	VehicleInfo[i][eVehicleParkInterior] = GetPlayerInterior(playerid); // interior of the player.
	VehicleInfo[i][eVehicleParkWorld] = GetPlayerVirtualWorld(playerid); // virtual world of the player.

	cache_delete(addVeh);
	SendMessage(playerid, "This car [id: %d | model: %d | virtual world: %d | interior: %d] has been added with success in our database.", i, vehModel, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid));
	return 1;
}

CMD:givegun(playerid, params[])
{
	new pID, gunID, gunAmmo;

	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(MasterInfo[playerid][masterAdmin] < 1)
		return SendUnauthMessage(playerid, "Only admin 1+ can use this command.");

	if(sscanf(params, "udd", pID, gunID, gunAmmo))
		return SendUsageMessage(playerid, "/givegun [playerid or name] [gun] [ammo]");

	SendClientMessage(playerid, COLOR_LIGHTRED, "_______________________________________");
	SendClientMessage(playerid, COLOR_LIGHTRED, "(1)Brass Knuckles (2)Golf Club (3)Nite Stick (4)Knife (5)Baseball Bat (6)Shovel (7)Pool Cue (8)Katana (9)Chainsaw");
	SendClientMessage(playerid, COLOR_LIGHTRED, "(10)Purple Dildo (11)Small White Vibrator (12)Large White Vibrator (13)Silver Vibrator (14)Flowers (15)Cane (16)Frag Grenade");
	SendClientMessage(playerid, COLOR_LIGHTRED, "(17)Tear Gas (18)Molotov Cocktail (22)9mm (23)Silenced 9mm (24)Desert Eagle (25)Shotgun (26)Sawnoff Shotgun");
	SendClientMessage(playerid, COLOR_LIGHTRED, "(27)Combat Shotgun (28)Micro SMG (Mac 10) (29)SMG (MP5) (30)AK-47 (31)M4 (32)Tec9 (33)Rifle (34)Sniper Rifle");
	SendClientMessage(playerid, COLOR_LIGHTRED, "(35)Rocket Launcher (36)HS Rocket Launcher (37)Flamethrower (38)Minigun (39)Satchel Charge (40)Detonator");
	SendClientMessage(playerid, COLOR_LIGHTRED, "(41)Spraycan (42)Fire Extinguisher (43)Camera (44)Nightvision Goggles (45)Infared Goggles (46)Parachute");
	SendClientMessage(playerid, COLOR_LIGHTRED, "_______________________________________");

	if(gunID < 1 || gunID > 47 || gunID == 21)
		return SendErrorMessage(playerid, "Invalid weapon ID!");

	if(IsPlayerConnected(pID))
	{
		if(pID != INVALID_PLAYER_ID && gunID <= 20 || gunID >= 22)
		{
			GivePlayerWeapon(pID, gunID, gunAmmo);
			SendServerMessage(playerid, "You have given %s a gun [ID: %d / Rounds: %d].", ReturnName(playerid, 0), gunID, gunAmmo);
		}
	}
	return 1;
}

CMD:adminhelp(playerid, params[])
{
    if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

    if(MasterInfo[playerid][masterAdmin] < 1)
        return SendUnauthMessage(playerid, "Only admin 5+ can use this command.");

    switch(MasterInfo[playerid][masterAdmin])
    {
        case 1:
        {
            SendMessage(playerid, "*  {BB9955}Admin Level 1 Commands{FFFFFF}  *");
            SendMessage(playerid, "{BB9955}chat:{FFFFFF} /gooc, /gdo, ");
			SendMessage(playerid, "{BB9955}movement/teleport:{FFFFFF} /goto, /gethere");
			SendMessage(playerid, "{BB9955}health:{FFFFFF} /sethp, /setarmour");
     	}
        case 2:
        {
            SendMessage(playerid, "*  {BB9955}Admin Level 1 Commands{FFFFFF}  *");
            SendMessage(playerid, "{BB9955}chat:{FFFFFF} /gooc, /gdo");
			SendMessage(playerid, "{BB9955}movement/teleport:{FFFFFF} /goto, /gethere");
			SendMessage(playerid, "{BB9955}health:{FFFFFF} /sethp, /setarmour");
        }
        case 3:
        {
            SendMessage(playerid, "*  {BB9955}Admin Level 1-3 Commands{FFFFFF}  *");
            SendMessage(playerid, "{BB9955}chat:{FFFFFF} /gooc, /gdo");
			SendMessage(playerid, "{BB9955}movement/teleport:{FFFFFF} /goto, /gethere");
			SendMessage(playerid, "{BB9955}health:{FFFFFF} /sethp, /setarmour");
        }
        case 4:
  		{
            SendMessage(playerid, "*  {BB9955}Admin Level 1-4 Commands{FFFFFF}  *");
            SendMessage(playerid, "{BB9955}chat:{FFFFFF} /gooc, /gdo");
			SendMessage(playerid, "{BB9955}movement/teleport:{FFFFFF} /flymode, /gotoliberty, /goto, /gethere");
			SendMessage(playerid, "{BB9955}vehicles:{FFFFFF} /createvehicle");
			SendMessage(playerid, "{BB9955}organizations:{FFFFFF} /createorganization");
			SendMessage(playerid, "{BB9955}weapons:{FFFFFF} /givegun");
			SendMessage(playerid, "{BB9955}health:{FFFFFF} /sethp, /setarmour");
			
        }
    }
    return 1;
}
alias:adminhelp("ahelp", "ah", "adminh");

CMD:gooc(playerid, params[])
{
	new globalOOCString[250],
		globalOOCText[250];

	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(MasterInfo[playerid][masterAdmin] < 1)
		return SendUnauthMessage(playerid, "Only admin 1+ can use this command.");

	if(sscanf(params, "s[250]", globalOOCText))
		return SendUsageMessage(playerid, "/gooc [global OOC]");

	format(globalOOCString, sizeof globalOOCString, "{BB9955}%s{FFFFFF}: (( %s ))", MasterInfo[playerid][masterName], globalOOCText);
	GlobalOOC(-1, globalOOCString);
	return 1;
}

CMD:sethp(playerid, params[])
{
	new id, hp;

	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(MasterInfo[playerid][masterAdmin] < 1)
		return SendUnauthMessage(playerid, "Only admin 1+ can use this command.");
	if(sscanf(params, "ui", id, hp))
		return SendUsageMessage(playerid, "/sethp [playerid] [health]");
	SetPlayerHealth(id, hp);
	return 1;
}

CMD:setarmour(playerid, params[])
{
	new id, armour;

	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(MasterInfo[playerid][masterAdmin] < 1)
		return SendUnauthMessage(playerid, "Only admin 1+ can use this command.");
	if(sscanf(params, "ui", id, armour))
		return SendUsageMessage(playerid, "/setarmour [playerid] [armour]");
	SetPlayerArmour(id, armour);
	return 1;
}

CMD:gdo(playerid, params[])
{
	new output[250],
		output2[250];
	
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(!MasterInfo[playerid][masterAdmin])
		return SendUnauthMessage(playerid, "Only admin 1+ can use this command.");
	
	if(isnull(params))
		return SendUsageMessage(playerid, "/gdo [text]");
		
	
	if(strlen(params) > 64)
	{
		format(output,sizeof(output), "* %.64s ...", params);
		format(output2,sizeof(output2),"... %s (( Global Announcement ))",params[64]);
		
		for (new i; i < MAX_PLAYERS; i++)
		{
			SendClientMessage(i, COLOR_PURPLE, output);
			SendClientMessage(i, COLOR_PURPLE, output2);
		}
	}

	else
	{
		format(output,sizeof(output),"* %s (( Global Announcement ))",params);
		for (new i; i < MAX_PLAYERS; i++)
		{
			SendClientMessage(i, COLOR_PURPLE, output);
		}
	}
	return 1;
}

CMD:flymode(playerid, params[])
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(MasterInfo[playerid][masterAdmin] < 1)
		return SendUnauthMessage(playerid, "Only admin 1+ can use this command.");

	if(IsPlayerInAnyVehicle(playerid))
	{
		new Float: pX, Float: pY, Float: pZ;
		GetVehicleVelocity(GetPlayerVehicleID(playerid), pX, pY, pZ);
		SetVehicleVelocity(GetPlayerVehicleID(playerid), pX, pY, pZ + 0.5);
	}
	if(FlyIsUsed[playerid] == false)
	{
		StartFly(playerid);
		FlyIsUsed[playerid] = true;
		SetPlayerHealth(playerid, 999999);
	}
	else if(FlyIsUsed[playerid] == true)
	{
		StopFly(playerid);
		FlyIsUsed[playerid] = false;
		SetPlayerHealth(playerid, 100);
	}
	return 1;
}
alias:flymode("fly");

CMD:gethere(playerid, params[])
{
	new Float: playerX,
		Float: playerY,
		Float: playerZ,
		toPlayer;

	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(MasterInfo[playerid][masterAdmin] < 1)
		return SendUnauthMessage(playerid, "Only admin 1+ can use this command.");

	if(sscanf(params, "u", toPlayer))
		return SendUsageMessage(playerid, "/gethere [playerid or name]");
	if(!IsPlayerConnected(toPlayer))
		return SendErrorMessage(playerid, "The player you specified isn't connected.");

	if(toPlayer == playerid)
		return SendErrorMessage(playerid, "You can't teleport to yourself.");

	GetPlayerPos(playerid, playerX, playerY, playerZ);
	SetPlayerPos(toPlayer, playerX + 1.5, playerY, playerZ);
	return 1;
}

CMD:goto(playerid, params[])
{
	new Float: toPlayerX,
		Float: toPlayerY, 
		Float: toPlayerZ,
		tPlayerID;

	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(MasterInfo[playerid][masterAdmin] < 1)
		return SendUnauthMessage(playerid, "Only admin 1+ can use this command.");

	if(sscanf(params, "u", tPlayerID))
		return SendUsageMessage(playerid, "/goto [playerid or name]");
	if(!IsPlayerConnected(tPlayerID))
		return SendErrorMessage(playerid, "The player you specified isn't connected.");

	if(tPlayerID == playerid)
		return SendErrorMessage(playerid, "You can't teleport to yourself.");

	GetPlayerPos(tPlayerID, toPlayerX, toPlayerZ, toPlayerY);
	SetPlayerPos(playerid, toPlayerX + 1.5, toPlayerZ, toPlayerY);
	return 1;
}
alias:goto("gotoplayer", "tp");

CMD:setskin(playerid, params[])
{
	new pID, skinID;
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(MasterInfo[playerid][masterAdmin] < 1)
		return SendUnauthMessage(playerid, "Only admin 1+ can use this command.");

	if(sscanf(params, "ui", pID, skinID))
		return SendUsageMessage(playerid, "/setskin [playerid or name] [skin id]");
	if(!IsPlayerConnected(pID))
		return SendErrorMessage(playerid, "The player you specified isn't connected.");

	SetPlayerSkin(pID, skinID);
	setCharacterInfoInt(pID, E_CHARACTERS:characterSkin, "characterSkin", skinID);
	SendServerMessage(playerid, "You set %s's skin to %d.", ReturnName(pID), skinID);
	SendClientMessageEx(pID, COLOR_LIGHTRED, "%s has set your skin to %d.", ReturnName(playerid), skinID);
	return 1;
}

CMD:ban(playerid, params[])
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(MasterInfo[playerid][masterAdmin] < 1)
		return SendUnauthMessage(playerid, "Only admin 1+ can use this command.");

	new playerBanned, banReason[120];

	if(sscanf(params, "us[120]", playerBanned, banReason))
		return SendUsageMessage(playerid, "/ban [playerid or name] [reason]");
	if(!IsPlayerConnected(playerBanned))
		return SendClientMessage(playerid, -1, "The player you specified isn't connected.");

	if(MasterInfo[playerBanned][masterAdmin] > MasterInfo[playerid][masterAdmin])
		return SendClientMessage(playerid, -1, "You can't ban an someone that has higher admin than you.");

	if(strlen(banReason) > 56)
	{
		SendClientMessageToAllEx(COLOR_LIGHTRED, "AdmCmd: %s was banned by %s, reason: %.56s", ReturnName(playerBanned), ReturnName(playerid), banReason);
		SendClientMessageToAllEx(COLOR_LIGHTRED, "AdmCmd: ... %s", banReason[56]);
	}
	else SendClientMessageToAllEx(COLOR_LIGHTRED, "AdmCmd: %s was banned by %s, reason: %s", ReturnName(playerBanned), ReturnName(playerid), banReason);

	new insertLog[256];
	if(MasterInfo[playerBanned][masterLogged] == false)
	{
		SendErrorMessage(playerid, "The player (%s) you selected isn't logged in.", ReturnName(playerBanned));
		return 1;
	}

	mysql_format(g_SQL, insertLog, sizeof insertLog, "INSERT INTO bannedlist (`bannedPlayer`, `bannedPlayerID`, `banReason`, `banDate`, `bannedBy`, `IPAddress`) VALUES ('%e', '%i', '%e', '%e', '%e', '%e')",
		MasterInfo[playerBanned][masterName], MasterInfo[playerBanned][masterID], banReason, ReturnDate(), MasterInfo[playerid][masterName], ReturnIP(playerBanned));
	mysql_tquery(g_SQL, insertLog);

	mysql_format(g_SQL, insertLog, sizeof insertLog, "INSERT INTO ban_logs (`bannedPlayer`, `bannedPlayerID`, `banReason`, `banDate`, `bannedBy`) VALUES ('%e', '%i', '%e', '%e', '%e')",
		MasterInfo[playerBanned][masterName], MasterInfo[playerBanned][masterID], banReason, ReturnDate(), MasterInfo[playerid][masterName]);
	mysql_tquery(g_SQL, insertLog);

	DelayedKick(playerBanned);
	return 1;
}

CMD:kick(playerid, params[])
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(MasterInfo[playerid][masterAdmin] < 1)
		return SendUnauthMessage(playerid, "Only admin 1+ can use this command."); 
		
	new playerb, reason[120];
	
	if (sscanf(params, "us[120]", playerb, reason)) 
		return SendUsageMessage(playerid, "/kick [playerid OR name] [reason]"); 
	
	if(!IsPlayerConnected(playerb))
		return SendErrorMessage(playerid, "The player you specified isn't connected."); 
		
	if(MasterInfo[playerid][masterAdmin] <= MasterInfo[playerb][masterAdmin]  )
		return SendErrorMessage(playerid, "You can't kick %s.", ReturnName(playerb)); 
		
	if(strlen(reason) > 56)
	{
		SendClientMessageToAllEx(COLOR_LIGHTRED, "AdmCmd: %s was kicked by %s, Reason: %.56s", ReturnName(playerb), ReturnName(playerid), reason);
		SendClientMessageToAllEx(COLOR_LIGHTRED, "AdmCmd: ...%s", reason[56]); 
	}
	else SendClientMessageToAllEx(COLOR_LIGHTRED, "AdmCmd: %s was kicked by %s, Reason: %s", ReturnName(playerb), ReturnName(playerid), reason);

	DelayedKick(playerb);
	return 1;
}