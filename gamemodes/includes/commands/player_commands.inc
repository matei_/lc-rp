/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

/* --- player commands --- */
CMD:help(playerid, params[])
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	SendClientMessage(playerid, -1, "* -- {BB9955}commands{FFFFFF} -- *");
	SendClientMessage(playerid, -1, "{BB9955}chat:{FFFFFF} (/low), (/whisper & /w), (/me & /melow), (/do & /dolow), (/my & /mylow)");
	SendClientMessage(playerid, -1, "{BB9955}chat:{FFFFFF} (/b), (/shout & /s)");
	return 1;
}

CMD:low(playerid, params[])
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(isnull(params))
		return SendUsageMessage(playerid, "/low [text]");

	if(strlen(params) > 64)
	{
		SendNearbyMessage(playerid, 10.0, -1, "%s says (low): %.64s ...", ReturnName(playerid, 0), params);
		SendNearbyMessage(playerid, 10.0, -1, "... %s", params[64]);
	}
	else
	{
		SendNearbyMessage(playerid, 10.0, -1, "%s says (low): %s", ReturnName(playerid, 0), params);
	}
	return 1;
}

CMD:whisper(playerid, params[])
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(isnull(params))
		return SendUsageMessage(playerid, "/whisper [text]");

	if(strlen(params) > 64)
	{
		SendNearbyMessage(playerid, 5.0, -1, "%s says (whisper): %.64s ...", ReturnName(playerid, 0), params);
		SendNearbyMessage(playerid, 5.0, -1, "... %s", params[64]);
	}
	else
	{
		SendNearbyMessage(playerid, 5.0, -1, "%s says (whisper): %s", ReturnName(playerid, 0), params);
	}
	return 1;
}
alias:whisper("w");

CMD:me(playerid, params[])
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(isnull(params))
		return SendUsageMessage(playerid, "/me [text]");

	if(strlen(params) > 64)
	{
		SendNearbyMessage(playerid, 30.0, COLOR_PURPLE, "* %s %.64s ...", ReturnName(playerid, 0), params);
		SendNearbyMessage(playerid, 30.0, COLOR_PURPLE, "... %s", params[64]);
	}
	else
	{
		SendNearbyMessage(playerid, 30.0, COLOR_PURPLE, "* %s %s", ReturnName(playerid, 0), params);
	}
	return 1;
}

CMD:melow(playerid, params[])
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(isnull(params))
		return SendUsageMessage(playerid, "/melow [text]");

	if(strlen(params) > 64)
	{
		SendNearbyMessage(playerid, 15.0, COLOR_PURPLE, "* %s %.64s ...", ReturnName(playerid, 0), params);
		SendNearbyMessage(playerid, 15.0, COLOR_PURPLE, "... %s", params[64]);
	}
	else
	{
		SendNearbyMessage(playerid, 15.0, COLOR_PURPLE, "* %s %s", ReturnName(playerid, 0), params);
	}
	return 1;
}

CMD:do(playerid, params[])
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(isnull(params))
		return SendUsageMessage(playerid, "/do [text]");

	if(strlen(params) > 64)
	{
		SendNearbyMessage(playerid, 30.0, COLOR_PURPLE, "* %.64s ...", params);
		SendNearbyMessage(playerid, 30.0, COLOR_PURPLE, "... %s (( %s ))", params[64], ReturnName(playerid, 0));
	}
	else
	{
		SendNearbyMessage(playerid, 30.0, COLOR_PURPLE, "* %s (( %s ))", params, ReturnName(playerid, 0));
	}
	return 1;
}


CMD:dolow(playerid, params[])
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(isnull(params))
		return SendUsageMessage(playerid, "/dolow [text]");

	if(strlen(params) > 64)
	{
		SendNearbyMessage(playerid, 15.0, COLOR_PURPLE, "* %.64s ...", params);
		SendNearbyMessage(playerid, 15.0, COLOR_PURPLE, "... %s (( %s ))", params[64], ReturnName(playerid, 0));
	}
	else
	{
		SendNearbyMessage(playerid, 15.0, COLOR_PURPLE, "* %s (( %s ))", params, ReturnName(playerid, 0));
	}
	return 1;
}

CMD:my(playerid, params[])
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(isnull(params))
		return SendUsageMessage(playerid, "/my [text]");

	if(strlen(params) > 64)
	{
		SendNearbyMessage(playerid, 30.0, COLOR_PURPLE, "* %s's %.64s ...", ReturnName(playerid, 0), params);
		SendNearbyMessage(playerid, 30.0, COLOR_PURPLE, "... %s", params[64]);
	}
	else
	{
		SendNearbyMessage(playerid, 30.0, COLOR_PURPLE, "* %s's %s", ReturnName(playerid, 0), params);
	}
	return 1;
}

CMD:mylow(playerid, params[])
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(isnull(params))
		return SendUsageMessage(playerid, "/mylow [text]");

	if(strlen(params) > 64)
	{
		SendNearbyMessage(playerid, 15.0, COLOR_PURPLE, "* %s's %.64s ...", ReturnName(playerid, 0), params);
		SendNearbyMessage(playerid, 15.0, COLOR_PURPLE, "... %s", params[64]);
	}
	else
	{
		SendNearbyMessage(playerid, 15.0, COLOR_PURPLE, "* %s's %s", ReturnName(playerid, 0), params);
	}
	return 1;
}

CMD:b(playerid, params[])
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(isnull(params))
		return SendUsageMessage(playerid, "/b [local OOC]");

	if(MasterInfo[playerid][masterAdmin] >= 1)
		if(strlen(params) > 64 )
		{
			SendNearbyMessage(playerid, 20.0, -1, "{BB9955}%s{FFFFFF} [master: {BB9955}%s{FFFFFF}]: (( %.64s ...", ReturnName(playerid, 0), MasterInfo[playerid][masterName], params);
			SendNearbyMessage(playerid, 20.0, -1, "... %s ))", params[64]);
		}
		else
		{
			SendNearbyMessage(playerid, 20.0, -1, "{BB9955}%s{FFFFFF} [master: {BB9955}%s{FFFFFF}]: (( %s ))", ReturnName(playerid, 0), MasterInfo[playerid][masterName], params);
		}
	else
	{
		if(strlen(params) > 64 )
		{
			SendNearbyMessage(playerid, 20.0, -1, "{BB9955}%s (%d){FFFFFF}: (( %.64s ...", ReturnName(playerid, 0), playerid, params);
			SendNearbyMessage(playerid, 20.0, -1, "... %s ))", params[64]);
		}
		else
		{
			SendNearbyMessage(playerid, 20.0, -1, "{BB9955}%s (%d){FFFFFF}: (( %s ))", ReturnName(playerid, 0), playerid, params);
		}
	}
	return 1;
}

CMD:shout(playerid, params[]) 
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(isnull(params))
		return SendUsageMessage(playerid, "/shout [text]");

	if(strlen(params) > 64)
	{
		SendNearbyMessage(playerid, 35.0, -1, "%s shouts: %.64s ...", ReturnName(playerid, 0), params);
		SendNearbyMessage(playerid, 35.0, -1, "... %s", params[64]);
	}
	else
	{
		SendNearbyMessage(playerid, 35.0, -1, "%s shouts: %s", ReturnName(playerid, 0), params);
	}
	return 1;
}
alias:shout("s");

CMD:pm(playerid, params[])
{
	new message[144], target;
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(sscanf(params, "us[144]", target, message))
		return SendUsageMessage(playerid, "/pm [id] [message]");

	SendClientMessageEx(playerid, COLOR_YELLOW, "(( PM to %s (%d): %s ))", ReturnName(target), target, message);
	SendClientMessageEx(target, COLOR_YELLOW, "(( PM from %s (%d): %s ))", ReturnName(playerid), playerid, message);
	return 1;
}

CMD:admins(playerid, params[])
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	new bool: adminOn = false;
	foreach(new i : Player)
	{
		if(MasterInfo[playerid][masterAdmin]) adminOn = true;
	}
	if(adminOn == true)
	{
		SendClientMessage(playerid, -1, "* Online Staff: *");
		foreach(new i : Player)
		{
			if(MasterInfo[i][masterAdmin] < 5)
			{
				SendClientMessageEx(playerid, -1, "(administration level: %d) %s (master: %s)", MasterInfo[i][masterAdmin], ReturnName(i), MasterInfo[i][masterName]);
			}
		}
	}
	else
	{
		return SendClientMessage(playerid, -1, "There are no administrators online.");
	}
	return 1;
}

CMD:id(playerid, params[])
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(isnull(params))
		return SendUsageMessage(playerid, "/id [playerid OR name]"); 
		
	new
		bool:inputID = false,
		playerb
	;
		
	for(new ix = 0, j = strlen(params); ix < j; ix++)
	{
		if (params[ix] > '9' || params[ix] < '0')
		{
			inputID = false; 
		}
		else inputID = true;
	}
	
	if(inputID)
	{
		playerb = strval(params);
		
		if(!IsPlayerConnected(playerb))
			return SendClientMessage(playerid, COLOR_LIGHTRED, "Player not found."); 
			
		SendClientMessageEx(playerid, COLOR_GREY, "(ID: %i) %s", playerb, ReturnName(playerb));
	}
	else
	{
		new
			bool:matchFound = false,
			bool:fullName = false,
			countMatches = 0,
			matchesFound[6],
			string[128]
		;
		
		for(new cc = 0; cc < 5; cc++) { matchesFound[cc] = INVALID_PLAYER_ID; }
		
		for(new i = 0, j = strlen(params); i < j; i++)
		{
			if (params[i] != '_')
			{
				fullName = false; 
			}
			else
			{
				fullName = true; 
			}
		}
		
		if(fullName)
		{
			foreach(new b : Player)
			{
				if(strfind(ReturnName(b), params, true) != -1)
				{
					SendClientMessageEx(playerid, COLOR_GREY, "(ID: %i) %s", b, ReturnName(b));
				}
				else return SendClientMessage(playerid, COLOR_LIGHTRED, "Player not found."); 
			}
		}
		else
		{
			for(new a = 0; a < MAX_PLAYERS; a++)
			{
				if(IsPlayerConnected(a))
				{
					if(strfind(ReturnName(a, 0), params, true) != -1)
					{
						matchFound = true;
						countMatches ++; 
					}
				}
			}
		
			if(matchFound)
			{
				for(new f = 0, g = GetPlayerPoolSize(), t = 0; f <= g; f++)
				{		
					if(IsPlayerConnected(f) && strfind(ReturnName(f, 0), params, true) != -1)
					{
						matchesFound[t] = f;
						t++; 
						
						if(t >= 5) break; 
					}
				}
			
				if(countMatches != 0 && countMatches > 1)
				{
					for(new l = 0; l < sizeof(matchesFound); l++)
					{
						if(matchesFound[l] == INVALID_PLAYER_ID)
							continue; 
							
						format(string, sizeof(string), "%s(ID: %i) %s, ", string, matchesFound[l], ReturnName(matchesFound[l])); 
											
						if(l % 3 == 0 && l != 0 || l == 5-1)
						{
							SendClientMessage(playerid, COLOR_GREY, string);
							string[0] = 0;
						}
					}
				}
				else if(countMatches == 1)
				{
					SendClientMessageEx(playerid, COLOR_GREY, "(ID: %i) %s", matchesFound[0], ReturnName(matchesFound[0]));
				}
			}
			else return SendClientMessage(playerid, COLOR_LIGHTRED, "Player not found."); 
		}
	}
	return 1; 
}