/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

/* --- faction functions --- */
stock ReturnFactionRank(playerid)
{
	new rankStr[256]; 

	if(CharacterInfo[playerid][characterFaction] == 0)
	{
		rankStr = "no rank";
	}
	else
	{  
		format(rankStr, sizeof(rankStr), "%s", CharacterInfo[playerid][characterRank]);
	}
	return rankStr;
}

function:LoadFactions()
{
	if(!cache_num_rows())
		return printf("[LC-RP MySQL]: Fatal error! No factions were loaded from our database: \"%s\".", MYSQL_DATABASE);

	new factionRows, factionFields;
	cache_get_data(factionRows, factionFields);

	for(new i = 0; i < factionRows && i < MAX_FACTIONS; i ++)
	{
		FactionInfo[i + 1][factionID] = cache_get_field_content_int(i, "factionID", g_SQL);
		FactionInfo[i + 1][factionType] = cache_get_field_content_int(i, "factionType", g_SQL);
		FactionInfo[i + 1][factionMaxMembers] = cache_get_field_content_int(i, "factionMaxMembers", g_SQL);
		FactionInfo[i + 1][factionBankNumber] = cache_get_field_content_int(i, "factionBankNumber", g_SQL);
		FactionInfo[i + 1][factionIsAdminCreated] = bool: cache_get_field_content_int(i, "factionIsAdminCreated", g_SQL);
		FactionInfo[i + 1][factionNumMembers] = cache_get_field_content_int(i, "factionNumMembers", g_SQL);

		cache_get_field_content(i, "factionName", FactionInfo[i + 1][factionName], g_SQL, 42);
		cache_get_field_content(i, "factionRankName1", FactionInfo[i + 1][factionRankName1], g_SQL, 32);
		cache_get_field_content(i, "factionRankName2", FactionInfo[i + 1][factionRankName2], g_SQL, 32);
		cache_get_field_content(i, "factionRankName3", FactionInfo[i + 1][factionRankName3], g_SQL, 32);
		cache_get_field_content(i, "factionRankName4", FactionInfo[i + 1][factionRankName4], g_SQL, 32);
		cache_get_field_content(i, "factionRankName5", FactionInfo[i + 1][factionRankName5], g_SQL, 32);
		cache_get_field_content(i, "factionRankName6", FactionInfo[i + 1][factionRankName6], g_SQL, 32);
		cache_get_field_content(i, "factionRankName7", FactionInfo[i + 1][factionRankName7], g_SQL, 32);
		cache_get_field_content(i, "factionRankName8", FactionInfo[i + 1][factionRankName8], g_SQL, 32);
		cache_get_field_content(i, "factionRankName9", FactionInfo[i + 1][factionRankName9], g_SQL, 32);
		cache_get_field_content(i, "factionRankName10", FactionInfo[i + 1][factionRankName10], g_SQL, 32);
	}
	printf("[LC-RP MySQL]: %i factions were loaded from our database: \"%s\".", factionRows, MYSQL_DATABASE);
	return 1;
}