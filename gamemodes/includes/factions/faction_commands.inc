/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

#include <YSI\y_hooks>

/* --- faction commands --- */
hook OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	switch(dialogid)
	{
		case DIALOG_FACTION_KICK_CONFIRM:
		{
			if(response == 1)
			{
				FACTION_KICK_OPTIONS[playerid][option_yes] = 1;
				return 1;
			}
			else
			{
				FACTION_KICK_OPTIONS[playerid][option_no] = 1;
				return 1;
			}
		}
		case DIALOG_FACTION_INVITE_CONFIRM:
		{
			if(response == 1)
			{
				FACTION_INVITE_OPTIONS[playerid][option_yes] = 1;
				return 1;
			}
			else
			{
				FACTION_INVITE_OPTIONS[playerid][option_no] = 1;
				return 1;
			}
		}
	}
	return 1;
}

CMD:faction(playerid, params[])
{
	new chatMessage[144];

	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(CharacterInfo[playerid][characterFaction] == 0)
		return SendErrorMessage(playerid, "You are not in a faction, so you can't use this command.");

	if(sscanf(params, "s[144]", chatMessage))
		return SendUsageMessage(playerid, "/faction [message]");

	foreach(new i : Player)
	{
		if(CharacterInfo[playerid][characterFaction] == CharacterInfo[i][characterFaction])
			SendMessage(i, "{00B8E6}** %s %s: (( %s ))", ReturnFactionRank(playerid), ReturnName(playerid, 0), chatMessage);
	}
	return 1;
}
alias:faction("f");

CMD:invite(playerid, params[])
{
	new invitedID, playerFaction, playerRank, targetFaction, uThread[256], inviteMessage[256];

	playerFaction = CharacterInfo[playerid][characterFaction];
	playerRank = CharacterInfo[playerid][characterRank];

	if(CharacterInfo[playerid][characterLoggedIn] == false)
		return SendUnauthMessage(playerid, "You must be logged in to use this command.");

	if(playerFaction == 0)
		return SendErrorMessage(playerid, "You are not in a faction, so you can not invite people in a faction.");

	if(sscanf(params, "u", invitedID))
		return SendUsageMessage(playerid, "/invite [playerid or name]");

	targetFaction = CharacterInfo[invitedID][characterFaction];

	if(playerFaction == targetFaction)
		return SendErrorMessage(playerid, "This player is already in your faction.");

	if((playerRank != FactionInfo[playerFaction][factionRankName1]) && (playerRank != FactionInfo[playerFaction][factionRankName2]))
		return SendErrorMessage(playerid, "Your rank is not authorized to invite members.");

	format(inviteMessage, sizeof(inviteMessage), "You have been invited to join %s!\n Accept invite?", FactionInfo[playerFaction][factionName]);
	ShowPlayerDialog(invitedID, DIALOG_FACTION_INVITE_CONFIRM, DIALOG_STYLE_MSGBOX, "Faction invite", inviteMessage, "Accept", "Decline");

	if(FACTION_INVITE_OPTIONS[invitedID][option_yes] == 1)
	{
		SendMessage(invitedID, "You joined %s!", FactionInfo[playerFaction][factionName]);

		FactionInfo[playerFaction][factionNumMembers] += 1;
		CharacterInfo[invitedID][characterFaction] = playerFaction;
		CharacterInfo[invitedID][characterRank] = FactionInfo[playerFaction][factionRankName10];

		mysql_format(g_SQL, uThread, sizeof(uThread), "UPDATE characters SET characterFaction = '%d', characterRank = '%s' WHERE characterID = '%i'", playerFaction, FactionInfo[playerFaction][factionRankName10], CharacterInfo[invitedID][characterID]);
		mysql_tquery(g_SQL, uThread);

		mysql_format(g_SQL, uThread, sizeof(uThread), "UPDATE factions SET factionNumMembers = '%d' WHERE factionID = '%d'", FactionInfo[playerFaction][factionNumMembers], CharacterInfo[playerFaction][factionID]);
		mysql_tquery(g_SQL, uThread);
	}

	FACTION_INVITE_OPTIONS[invitedID][option_yes] = 0;
	FACTION_INVITE_OPTIONS[invitedID][option_no] = 0;
	return 1;
}