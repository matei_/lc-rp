/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

/* --- vehicle functions --- */
function:LoadVehicles()
{
	if(!cache_num_rows())
		return printf("[LC-RP MySQL]: Fatal error! No vehicles were loaded from our database: \"%s\".", MYSQL_DATABASE);

	new vehicleRows, vehicleFields, endCount = 0, vehString[128], vehicleid = INVALID_VEHICLE_ID;
	cache_get_data(vehicleRows, vehicleFields);

	for (new i = 0; i < vehicleRows && i < MAX_VEHICLES; i ++)
	{
		vehicleid = CreateVehicle(cache_get_field_content_int(i, "VehicleModel", g_SQL), 
			cache_get_field_content_float(i, "VehicleParkPosX", g_SQL),
			cache_get_field_content_float(i, "VehicleParkPosY", g_SQL),
			cache_get_field_content_float(i, "VehicleParkPosZ", g_SQL),
			cache_get_field_content_float(i, "VehicleParkPosA", g_SQL),
			cache_get_field_content_int(i, "VehicleColor1", g_SQL),
			cache_get_field_content_int(i, "VehicleColor2", g_SQL),
			-1,
			cache_get_field_content_int(i, "VehicleSirens", g_SQL));
			
		if(vehicleid != INVALID_VEHICLE_ID)
		{
			VehicleInfo[vehicleid][eVehicleExists] = true; 
			VehicleInfo[vehicleid][eVehicleID] = cache_get_field_content_int(i, "VehicleID", g_SQL); 
			
			VehicleInfo[vehicleid][eVehicleOwnerID] = cache_get_field_content_int(i, "VehicleOwnerID", g_SQL);
			VehicleInfo[vehicleid][eVehicleFaction] = cache_get_field_content_int(i, "VehicleFaction", g_SQL); 
			
			VehicleInfo[vehicleid][eVehicleModel] = cache_get_field_content_int(i, "VehicleModel", g_SQL);
			
			VehicleInfo[vehicleid][eVehicleColor1] = cache_get_field_content_int(i, "VehicleColor1", g_SQL);
			VehicleInfo[vehicleid][eVehicleColor2] = cache_get_field_content_int(i, "VehicleColor2", g_SQL);
			
			VehicleInfo[vehicleid][eVehicleParkPos][0] = cache_get_field_content_float(i, "VehicleParkPosX", g_SQL);
			VehicleInfo[vehicleid][eVehicleParkPos][1] = cache_get_field_content_float(i, "VehicleParkPosY", g_SQL);
			VehicleInfo[vehicleid][eVehicleParkPos][2] = cache_get_field_content_float(i, "VehicleParkPosZ", g_SQL);
			VehicleInfo[vehicleid][eVehicleParkPos][3] = cache_get_field_content_float(i, "VehicleParkPosA", g_SQL);
			
			VehicleInfo[vehicleid][eVehicleParkInterior] = cache_get_field_content_int(i, "VehicleParkInterior", g_SQL);
			VehicleInfo[vehicleid][eVehicleParkWorld] = cache_get_field_content_int(i, "VehicleParkWorld", g_SQL);
			
			cache_get_field_content(i, "VehiclePlates", VehicleInfo[vehicleid][eVehiclePlates], g_SQL, 32);
			VehicleInfo[vehicleid][eVehicleLocked] = bool: cache_get_field_content_int(i, "VehicleLocked", g_SQL);
			
			VehicleInfo[vehicleid][eVehicleImpounded] = bool: cache_get_field_content_int(i, "VehicleImpounded", g_SQL);
			
			VehicleInfo[vehicleid][eVehicleImpoundPos][0] = cache_get_field_content_float(i, "VehicleImpoundPosX", g_SQL);
			VehicleInfo[vehicleid][eVehicleImpoundPos][1] = cache_get_field_content_float(i, "VehicleImpoundPosY", g_SQL);
			VehicleInfo[vehicleid][eVehicleImpoundPos][2] = cache_get_field_content_float(i, "VehicleImpoundPosZ", g_SQL);
			VehicleInfo[vehicleid][eVehicleImpoundPos][3] = cache_get_field_content_float(i, "VehicleImpoundPosA", g_SQL);
			
			VehicleInfo[vehicleid][eVehicleFuel] = cache_get_field_content_int(i, "VehicleFuel", g_SQL); 
			VehicleInfo[vehicleid][eVehicleSirens] = cache_get_field_content_int(i, "VehicleSirens", g_SQL);
			
			VehicleInfo[vehicleid][eVehicleHasXMR] = bool: cache_get_field_content_int(i, "VehicleXMR", g_SQL);
			VehicleInfo[vehicleid][eVehicleTimesDestroyed] = cache_get_field_content_int(i, "VehicleTimesDestroyed", g_SQL);
						
			VehicleInfo[vehicleid][eVehicleEngine] = cache_get_field_content_float(i, "VehicleEngine", g_SQL);
			VehicleInfo[vehicleid][eVehicleBattery] = cache_get_field_content_float(i, "VehicleBattery", g_SQL);
			
			VehicleInfo[vehicleid][eVehicleAlarmLevel] = cache_get_field_content_int(i, "VehicleAlarmLevel", g_SQL);
			VehicleInfo[vehicleid][eVehicleLockLevel] = cache_get_field_content_int(i, "VehicleLockLevel", g_SQL);
			VehicleInfo[vehicleid][eVehicleImmobLevel] = cache_get_field_content_int(i, "VehicleImmobLevel", g_SQL);
			
			for(new j = 1; j < 6; j++)
			{
				format(vehString, sizeof(vehString), "VehicleWeapons%d", j);
				VehicleInfo[vehicleid][eVehicleWeapons][j] = cache_get_field_content_int(i, vehString, g_SQL);
				
				format(vehString, sizeof(vehString), "VehicleWeaponsAmmo%d", j);
				VehicleInfo[vehicleid][eVehicleWeaponsAmmo][j] = cache_get_field_content_int(i, vehString, g_SQL);
			}
			
			for(new d = 1; d < 5; d++)
			{
				format(vehString, sizeof(vehString), "VehicleLastDrivers%d", d);
				VehicleInfo[vehicleid][eVehicleLastDrivers][d] = cache_get_field_content_int(i, vehString, g_SQL);
				
				format(vehString, sizeof(vehString), "VehicleLastPassengers%d", d);
				VehicleInfo[vehicleid][eVehicleLastPassengers][d] = cache_get_field_content_int(i, vehString, g_SQL);
			}
			
			if(VehicleInfo[vehicleid][eVehicleParkInterior] != 0)
			{
				LinkVehicleToInterior(vehicleid, VehicleInfo[vehicleid][eVehicleParkInterior]); 
				SetVehicleVirtualWorld(vehicleid, VehicleInfo[vehicleid][eVehicleParkWorld]);
			}
			
			if(!isnull(VehicleInfo[vehicleid][eVehiclePlates]))
			{
				SetVehicleNumberPlate(vehicleid, VehicleInfo[vehicleid][eVehiclePlates]);
				SetVehicleToRespawn(vehicleid); 
			}
			
			if(VehicleInfo[vehicleid][eVehicleImpounded] == true)
			{
				SetVehiclePos(vehicleid, VehicleInfo[vehicleid][eVehicleImpoundPos][0], VehicleInfo[vehicleid][eVehicleImpoundPos][1], VehicleInfo[vehicleid][eVehicleImpoundPos][2]);
				SetVehicleZAngle(vehicleid, VehicleInfo[vehicleid][eVehicleImpoundPos][3]); 
			}
			
			VehicleInfo[vehicleid][eVehicleAdminSpawn] = false;
			endCount++;
		}
	}
	printf("[LC-RP MySQL]: %d vehicles were loaded from our database: \"%s\".", vehicleRows, MYSQL_DATABASE);
	return 1;
}