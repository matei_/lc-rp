/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

/* --- vehicles commands --- */
CMD:engine(playerid, params[])
{
	new vehEngine, vehAlarm, vehLights, vehDoors, vehBonnet, vehBoot, vehObjective, vehicleID = GetPlayerVehicleID(playerid);

	if(!IsPlayerInAnyVehicle(playerid))
		return SendErrorMessage(playerid, "You are not in any vehicle.");

	GetVehicleParamsEx(vehicleID, vehEngine, vehLights, vehAlarm, vehDoors, vehBonnet, vehBoot, vehObjective);
	if(GetPlayerState(playerid) == 2)
	{
		if(vehEngine != 1)
		{
			SendNearbyMessage(playerid, 30.0, COLOR_PURPLE, "* %s turns on the vehicle's engine.", ReturnName(playerid, 0));
			SetVehicleParamsEx(vehicleID, 1, 1, vehAlarm, vehDoors, vehBonnet, vehBoot, vehObjective);
		}
		else if(vehEngine != 0)
		{
			SendNearbyMessage(playerid, 30.0, COLOR_PURPLE, "* %s turns the vehicle's engine off.", ReturnName(playerid, 0));
			SetVehicleParamsEx(vehicleID, 0, 0, vehAlarm, vehDoors, vehBonnet, vehBoot, vehObjective);
		}
	}
	return 1;
}