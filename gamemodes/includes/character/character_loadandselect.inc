/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

#include <YSI\y_hooks>

/* --- character load and select --- */
hook OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	switch(dialogid)
	{
		case DIALOG_CHARACTERS:
		{
			if(!response)
			{
				SendServerMessage(playerid, "You have been kicked for not selecting a character to play.");
				DelayedKick(playerid);
			}

			if(strfind(CharacterLister[playerid][listitem], "[", false) != -1)
			{
				for(new i = 0; i < 3; i ++) {	}
				SendErrorMessage(playerid, "This character is not valid (or not completly created).");
				DelayedKick(playerid);
			}
			else
			{
				new characterThread[128];

				SendMessage(playerid, "You have selected the character: {BB9955}%s{FFFFFF}.", CharacterLister[playerid][listitem]);

				SetPlayerName(playerid, CharacterLister[playerid][listitem]);

				mysql_format(g_SQL, characterThread, sizeof(characterThread), "SELECT * FROM characters WHERE characterName = '%e' LIMIT 1", CharacterLister[playerid][listitem]);
				mysql_tquery(g_SQL, characterThread, "SelectCharacter", "i", playerid);
			}
			return 1;
		}
	}
	return 1;
}

function:ListCharacters(playerid)
{
	if(!cache_num_rows())
	{
		new listStrings[129];

		for(new i = 0; i < 3; i ++)
		{
			CharacterLister[playerid][i] = "no character available";
			format(listStrings, sizeof(listStrings), "%s%s\n", listStrings, CharacterLister[playerid][i]);
		}
		return ShowPlayerDialog(playerid, DIALOG_CHARACTERS, DIALOG_STYLE_LIST, "Select your character:", listStrings, "Select", "Abort");
	}

	if(MasterInfo[playerid][masterLogged] == true)
	{
		SendMessage(playerid, "You have successfully logged in as: {BB9955}%s{FFFFFF}.", ReturnName(playerid));
		MasterInfo[playerid][masterLogged] = false;
		SetPlayerCamera(playerid);
	}

	new characterRows, characterFields;
	cache_get_data(characterRows, characterFields);

	if(characterRows)
	{
		new listString[129];

		for(new i = 0; i < characterRows; i ++)
		{
			cache_get_field_content(i, "characterName", CharacterLister[playerid][i], g_SQL, 129);
		}

		for(new i = 0; i < 3; i ++)
		{
			if(isnull(CharacterLister[playerid][i]))
			{
				CharacterLister[playerid][i] = "no character available";
			}
			format(listString, sizeof(listString), "%s%s\n", listString, CharacterLister[playerid][i]);
		}
		ShowPlayerDialog(playerid, DIALOG_CHARACTERS, DIALOG_STYLE_LIST, "Select your character:", listString, "Select", "Abort");
	}
	return 1;
}

function:SelectCharacter(playerid)
{
	if(!cache_num_rows())
	{
		SendErrorMessage(playerid, "An error eccurred processing your character. Returning to your character list...");

		new listString[129];

		for(new i = 0; i < 3; i ++) {		format(listString, sizeof(listString), "%s%s\n", listString, CharacterLister[playerid][i]);		}
		return ShowPlayerDialog(playerid, DIALOG_CHARACTERS, DIALOG_STYLE_LIST, "Select your character:", listString, "Select", "Abort");
	}

	new charRows, charFields, charThread[80];
	cache_get_data(charRows, charFields);

	if(charRows)
	{
		mysql_format(g_SQL, charThread, sizeof(charThread), "SELECT * FROM characters WHERE characterName = '%e'", ReturnName(playerid));
		mysql_tquery(g_SQL, charThread, "Query_LoadCharacter", "i", playerid);
	}
	return 1;
}

function:Query_LoadCharacter(playerid)
{
	CharacterInfo[playerid][characterID] = cache_get_field_content_int(0, "characterID");
	CharacterInfo[playerid][characterAge] = cache_get_field_content_int(0, "characterAge");
	CharacterInfo[playerid][characterSex] = cache_get_field_content_int(0, "characterSex");
	CharacterInfo[playerid][characterSkin] = cache_get_field_content_int(0, "characterSkin");
	CharacterInfo[playerid][characterDead] = cache_get_field_content_int(0, "characterDead");
	CharacterInfo[playerid][characterInjured] = cache_get_field_content_int(0, "characterInjured");
	CharacterInfo[playerid][characterFaction] = cache_get_field_content_int(0, "characterFaction");

	cache_get_field_content(0, "characterRank", CharacterInfo[playerid][characterRank], g_SQL, 256);

	CharacterInfo[playerid][characterPos][0] = cache_get_field_content_float(0, "characterX");
	CharacterInfo[playerid][characterPos][1] = cache_get_field_content_float(0, "characterY");
	CharacterInfo[playerid][characterPos][2] = cache_get_field_content_float(0, "characterZ");

	TogglePlayerSpectating(playerid, false);
	return LoadCharacter(playerid);
}

function:LoadCharacter(playerid)
{
	CharacterInfo[playerid][characterLoggedIn] = true;
	SetPlayerColor(playerid, 0xFFFFFFFF);

	SetSpawnInfo(playerid, 0, CharacterInfo[playerid][characterSkin], CharacterInfo[playerid][characterPos][0], CharacterInfo[playerid][characterPos][1], CharacterInfo[playerid][characterPos][2], 0.0, 0, 0, 0, 0, 0, 0);
	SetPlayerSkin(playerid, CharacterInfo[playerid][characterSkin]);
	SpawnPlayer(playerid);

	new characterWelcome[90];
	format(characterWelcome, sizeof(characterWelcome), "~w~welcome~n~~y~ %s", ReturnName(playerid, 0));
	GameTextForPlayer(playerid, characterWelcome, 1000, 4);
	return 1;
}

function:QUERY_ListCharacters(playerid)
{
	new fetchChars[128];
	mysql_format(g_SQL, fetchChars, sizeof(fetchChars), "SELECT characterName, characterID FROM characters WHERE masterID = %i LIMIT 3", MasterInfo[playerid][masterID]);
	mysql_pquery(g_SQL, fetchChars, "ListCharacters", "i", playerid);
	GameTextForPlayer(playerid, "~y~NOW, SELECT YOUR CHARACTER", 2500, 4);
	return 1;
}