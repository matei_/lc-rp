/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

/* --- character functions --- */
stock setCharacterInfoInt(playerid, E_CHARACTERS:characterVar, dbField[32], cValue)
{
	new characterIntString[129];
	CharacterInfo[playerid][characterVar] = cValue;
	mysql_format(g_SQL, characterIntString, sizeof(characterIntString), "UPDATE characters SET %s = '%d' WHERE characterID = '%i' LIMIT 1", dbField, CharacterInfo[playerid][characterVar], CharacterInfo[playerid][characterID]);
	mysql_pquery(g_SQL, characterIntString, "", "");
}

function:UpdateCharacterPosition(playerid)
{
	new uThread[256];

	GetPlayerPos(playerid, CharacterInfo[playerid][characterPos][0], CharacterInfo[playerid][characterPos][1], CharacterInfo[playerid][characterPos][2]);

	mysql_format(g_SQL, uThread, sizeof uThread, "UPDATE characters SET characterX = %f, characterY = %f, characterZ = %f WHERE characterID = %i",

		CharacterInfo[playerid][characterPos][0],
		CharacterInfo[playerid][characterPos][1],
		CharacterInfo[playerid][characterPos][2],
		CharacterInfo[playerid][characterID]);
	return mysql_tquery(g_SQL, uThread);
}
