/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

/* --- server variables --- */
new CharacterInfo[MAX_PLAYERS][E_CHARACTERS],
	CharacterLister[MAX_PLAYERS][3][MAX_PLAYER_NAME + 1];

new MasterInfo[MAX_PLAYERS][E_MASTER],
	g_MySQLRaceCheck[MAX_PLAYERS];

new FactionInfo[MAX_FACTIONS][E_FACTIONS],
	VehicleInfo[MAX_VEHICLES][E_VEHICLES];
	
new FACTION_KICK_OPTIONS[MAX_PLAYERS][E_FACTION_KICK_OPTIONS],
	FACTION_INVITE_OPTIONS[MAX_PLAYERS][E_FACTION_INVITE_OPTIONS];

new bool: FlyIsUsed[MAX_PLAYERS],
	globalWeather = 2,
	g_SQL = -1;