/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

/* --- server functions --- */
function:GlobalOOC(color, const string[])
{
	foreach(new i : Player)
	{
		SendClientMessage(i, color, string);
	}
	return 1;
}

function:SetPlayerCamera(playerid)
{
	new randCamera = random(3);

	switch(randCamera)
	{
		case 0:
		{
			SetPlayerCameraPos(playerid, 1249.3018, -1697.8046, 99.9554);
			SetPlayerCameraLookAt(playerid, 1249.6576, -1696.8656, 99.4902);
		}
		case 1:
		{
			SetPlayerCameraPos(playerid, 2151.2539, -1894.5447, 85.3924);
			SetPlayerCameraLookAt(playerid, 2150.5833, -1893.8066, 84.6774);
		}
		case 2:
		{
			SetPlayerCameraPos(playerid, 2169.0635, -1740.4182, 112.0308);
			SetPlayerCameraLookAt(playerid, 2170.0603, -1740.3655, 111.3108);
		}
	}
	return 1;
}

function:PreloadAnimLib(playerid, animlib[]) return ApplyAnimation(playerid, animlib, "null", 0.0, 0, 0, 0, 0, 0, 0);

/* --- server stocks --- */
stock ReturnName(playerid, underScore = 1)
{
	new playersName[MAX_PLAYER_NAME + 2];
	GetPlayerName(playerid, playersName, sizeof playersName);

	if(!underScore)
	{
		for(new i = 0, j = strlen(playersName); i < j; i ++)
		{
			if(playersName[i] == '_')
			{
				playersName[i] = ' ';
			}
		}
	}
	return playersName;
}

stock ReturnDate()
{
	new sendString[90], monthString[40], month, day, year;
	new hour, minute, second;

	gettime(hour, minute, second);
	getdate(year, month, day);

	switch(month)
	{
		case 1:  monthString = "January";
		case 2:  monthString = "February";
		case 3:  monthString = "March";
		case 4:  monthString = "April";
		case 5:  monthString = "May";
		case 6:  monthString = "June";
		case 7:  monthString = "July";
		case 8:  monthString = "August";
		case 9:  monthString = "September";
		case 10: monthString = "October";
		case 11: monthString = "November";
		case 12: monthString = "December";
	}

	format(sendString, sizeof sendString, "%d/%s/%d - %02d:%02d:%02d", day, monthString, year, hour, minute, second);
	return sendString;
}

stock ReturnIP(playerid)
{
	new ipAddress[20];
	GetPlayerIp(playerid, ipAddress, sizeof ipAddress);
	return ipAddress;
}

stock SendNearbyMessage(playerid, Float: radius, color, const str[], {Float,_}:...) 
{
	static
	    args,
	    start,
	    end,
	    string[144]
	;
	#emit LOAD.S.pri 8
	#emit STOR.pri args

	if (args > 16)
	{
		#emit ADDR.pri str
		#emit STOR.pri start

	    for (end = start + (args - 16); end > start; end -= 4)
		{
	        #emit LREF.pri end
	        #emit PUSH.pri
		}
		#emit PUSH.S str
		#emit PUSH.C 144
		#emit PUSH.C string

		#emit LOAD.S.pri 8
		#emit CONST.alt 4
		#emit SUB
		#emit PUSH.pri

		#emit SYSREQ.C format
		#emit LCTRL 5
		#emit SCTRL 4

        foreach (new i : Player)
		{
			if (IsPlayerNearPlayer(i, playerid, radius)) {
  				SendClientMessage(i, color, string);
			}
		}
		return 1;
	}
	foreach (new i : Player)
	{
		if (IsPlayerNearPlayer(i, playerid, radius)) {
			SendClientMessage(i, color, str);
		}
	}
	return 1;
}

stock SendClientMessageEx(playerid, color, const str[], {Float,_}:...)
{
	static
	    args,
	    start,
	    end,
	    string[156]
	;
	#emit LOAD.S.pri 8
	#emit STOR.pri args

	if (args > 12)
	{
		#emit ADDR.pri str
		#emit STOR.pri start

	    for (end = start + (args - 12); end > start; end -= 4)
		{
	        #emit LREF.pri end
	        #emit PUSH.pri
		}
		#emit PUSH.S str
		#emit PUSH.C 156
		#emit PUSH.C string
		#emit PUSH.C args
		#emit SYSREQ.C format

		SendClientMessage(playerid, color, string);

		#emit LCTRL 5
		#emit SCTRL 4
		#emit RETN
	}
	return SendClientMessage(playerid, color, str);
} // Credits to Emmet, South Central Roleplay

stock SendClientMessageToAllEx(color, const str[], {Float,_}:...)
{
	static
	    args,
	    start,
	    end,
	    string[144]
	;
	#emit LOAD.S.pri 8
	#emit STOR.pri args

	if (args > 8)
	{
		#emit ADDR.pri str
		#emit STOR.pri start

	    for (end = start + (args - 8); end > start; end -= 4)
		{
	        #emit LREF.pri end
	        #emit PUSH.pri
		}
		#emit PUSH.S str
		#emit PUSH.C 144
		#emit PUSH.C string

		#emit LOAD.pri args
		#emit ADD.C 4
		#emit PUSH.pri
		#emit SYSREQ.C format

        #emit LCTRL 5
		#emit SCTRL 4

		foreach (new i : Player) {
			SendClientMessage(i, color, string);
		}
		return 1;
	}
	return SendClientMessageToAll(color, str);
} 

stock IsPlayerNearPlayer(playerid, targetid, Float: radius)
{
	static
		Float: fX,
		Float: fY,
		Float: fZ;

	GetPlayerPos(targetid, fX, fY, fZ);
	return(GetPlayerInterior(playerid) == GetPlayerInterior(targetid) && GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(targetid)) && IsPlayerInRangeOfPoint(playerid, radius, fX, fY, fZ);
}