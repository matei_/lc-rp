/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

/* --- server_defines --- */

	/* --- natives --- */
		native WP_Hash(buffer[], len, const str[]);
		native IsValidVehicle(vehicleid);

	/* --- mysql --- */
		#define MYSQL_HOST \
			"localhost"

		#define MYSQL_USER \
			"root"

		#define MYSQL_PASSWORD \
			""

		#define MYSQL_DATABASE \
			"03dl"

	/* --- keys defiens --- */
		#define PRESSED(%0) \
			(((newkeys & (%0)) == (%0)) && ((oldkeys & (%0)) != (%0)))

		#define RELEASED(%0) \
			(((newkeys & (%0)) != (%0)) && ((oldkeys & (%0)) == (%0)))

		#define HOLDING(%0) \
			((newkeys & (%0)) == (%0))

	/* --- shortcuts --- */
		#define function:%0(%1) \
			forward %0(%1); public %0(%1)

		#define SendUsageMessage(%0,%1) \
			SendClientMessageEx(%0, COLOR_LIGHTRED, "USAGE: {FFFFFF}"%1)

		#define SendErrorMessage(%0,%1) \
			SendClientMessageEx(%0, COLOR_LIGHTRED, "ERROR: {FFFFFF}"%1)

		#define SendServerMessage(%0,%1) \
			SendClientMessageEx(%0, COLOR_LIGHTRED, "SERVER: {FFFFFF}"%1)

		#define SendMessage(%0,%1) \
			SendClientMessageEx(%0, COLOR_WHITE, %1)

		#define UnknownMessage(%0,%1) \
			SendClientMessageEx(%0, COLOR_LIGHTRED, "Unknown Command: {FFFFFF}"%1)

		#define SendAdminMessage(%0,%1) \
			SendClientMessageEx(%0, COLOR_LIGHTRED, "ADMCMD: {FFFFFF}"%1)

		#define SendWarningMessage(%0,%1) \
			SendClientMessageEx(%0, COLOR_LIGHTRED, "WARNING: {FFFFFF}"%1)

		#define SendUnauthMessage(%0,%1) \
			SendClientMessageEx(%0, COLOR_LIGHTRED, "UNAUTHORIZED: {FFFFFF}"%1)

	/* --- colors --- */
		#define 	COLOR_PURPLE		0xD0AEEBFF
		#define 	COLOR_LIGHTRED		0xFF6347AA
		#define 	COLOR_YELLOW		0xFFFF00FF
		#define 	COLOR_BLUE			0x2641FEFF
		#define 	COLOR_GRAD6 		0xF0F0F0FF
		#define 	COLOR_BROWN 		0x993300AA
		#define 	COLOR_GREY 			0xAFAFAFAA
		#define 	COLOR_GREEN 		0x33AA33AA
		#define 	COLOR_WHITE 		0xFFFFFFFF

	/* --- defines --- */
		#define 	MAX_FACTIONS			20