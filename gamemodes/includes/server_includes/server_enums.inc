/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

/* --- server enums --- */
	
	/* --- character enum --- */
		enum E_CHARACTERS
		{
			characterID,
			characterMasterID,
			characterAge,
			characterSkin,
			characterSex,
			characterDead,
			characterInjured,
			characterFaction,
			characterRank[128],

			Float: characterPos[3],

			bool: characterLoggedIn
		};

	/* --- master enum --- */
		enum E_MASTER
		{
			masterID,
			masterName[MAX_PLAYER_NAME],
			masterPassword[129],
			masterEmail[255],
			masterSecretQ[255],
			masterSecretA[255],
			masterAdmin,
			masterDonorLevel,
			masterDonorDate,
			masterLoginAttempts,

			bool: masterIsVerified,
			bool: masterIsDonor,
			bool: masterLogged,
			bool: masterRegistered
		};

	/* --- faction enum --- */
		enum E_FACTIONS
		{
			factionID,
			factionName[32],
			factionType,
			factionMaxMembers,
			factionRankName1[32],
			factionRankName2[32],
			factionRankName3[32],
			factionRankName4[32],
			factionRankName5[32],
			factionRankName6[32],
			factionRankName7[32],
			factionRankName8[32],
			factionRankName9[32],
			factionRankName10[32],
			factionBankNumber,
			factionNumMembers,
			factionIsAdminCreated
		};

	/* -- vehicles enum --- */
		enum E_VEHICLES
		{
			eVehicleID,
			eVehicleOwnerID,
			eVehicleFaction,
			eVehicleModel,
			eVehicleColor1,
			eVehicleColor2,
			eVehiclePaintjob,
			eVehicleParkInterior,
			eVehicleParkWorld,
			eVehiclePlates[32],
			eVehicleFuel,
			eVehicleSirens,
			eVehicleTowCount,
			eVehicleXMRURL[128],
			eVehicleTimesDestroyed,
			eVehicleLockLevel,
			eVehicleAlarmLevel,
			eVehicleImmobLevel,
			eVehicleEnterTimer,
			eVehicleRefillCount,
			eVehicleLastDrivers[5], // last four drivers.
			eVehicleLastPassengers[5], // last four passangers.
			eVehicleWeapons[6], // five weapons.
			eVehicleWeaponsAmmo[6], // five weapons ammo.

			Float: eVehicleParkPos[4],
			Float: eVehicleBattery,
			Float: eVehicleEngine,
			Float: eVehicleImpoundPos[4],

			bool: eVehicleExists,
			bool: eVehicleLocked,
			bool: eVehicleLights,
			bool: eVehicleEngineStatus,
			bool: eVehicleAdminSpawn,
			bool: eVehicleHasXMR,
			bool: eVehicleXMRON,
			bool: eVehicleHasCarsign,
			bool: eVehicleImpounded,

			Text3D: eVehicleTowDisplay,
			Text3D: eVehicleEnterTD,
			Text3D: eVehicleCarsign,
			Text3D: eVehicleRefillDisplay
		};

	/* --- dialog enum --- */
		enum
		{
			DIALOG_INVALID,
			DIALOG_UNUSED,

			DIALOG_LOGIN,
			DIALOG_REGISTER,
			DIALOG_CHARACTERS,
			
			DIALOG_FACTION_INVITE_CONFIRM,
			DIALOG_FACTION_KICK_CONFIRM
		};

	/* --- faction enum --- */
		enum E_FACTION_KICK_OPTIONS
		{
			option_yes,
			option_no
		};

		enum E_FACTION_INVITE_OPTIONS
		{
			option_yes,
			option_no
		};