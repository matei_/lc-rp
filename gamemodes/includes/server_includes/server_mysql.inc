/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

/* --- server mysql --- */
stock g_mysql_init()
{
	g_SQL = mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_DATABASE, MYSQL_PASSWORD);
	printf("[LC-RP MySQL]: Connecting to %s ...", MYSQL_DATABASE);

	mysql_log(LOG_ALL, LOG_TYPE_HTML);

	if(mysql_errno(g_SQL) != 0)
	{
		printf("[LC-RP MySQL]: Fatal Error! Could not connect to MySQL: host %s - DB: %s - USER: %s", MYSQL_HOST, MYSQL_DATABASE, MYSQL_USER);
		print("[LC-RP MySQL]: Make sure that you have provided the correct connection credentials.");
		printf("[LC-RP MySQL]: Error number: %d", mysql_errno(g_SQL));
		SendRconCommand("exit");
	}
	else print("[LC-RP MySQL]: Connection successful toward MySQL Database Server.");

	InitiateGamemode();
	return 1;
}

stock g_mysql_exit()
{
	mysql_close(g_SQL);
	return 1;
}

InitiateGamemode()
{
	ShowPlayerMarkers(PLAYER_MARKERS_MODE_OFF);
	SetNameTagDrawDistance(20.0);

	EnableStuntBonusForAll(0);
	
	ManualVehicleEngineAndLights();
	DisableInteriorEnterExits();
	AllowInteriorWeapons(1);

	SetWeather(globalWeather);

	new wHour, wSeconds, wMinute;
	gettime(wHour, wMinute, wSeconds);
	SetWorldTime(wHour);
	
	SetGameModeText(SERVER_VERSION);

	print("\n-------------------------------------------");
	print("Liberty City Roleplay");
	print("Copyright (C) Liberty City Roleplay, LCR (2018 - 2019)");
	print("All rights reserverd.");
	print("-------------------------------------------\n");
	print("Successfully initiated the gamemode.");

	mysql_tquery(g_SQL, "SELECT * FROM factions ORDER BY factionID ASC", "LoadFactions");
	mysql_tquery(g_SQL, "SELECT * FROM vehicles ORDER BY vehicleID ASC", "LoadVehicles");
	mysql_tquery(g_SQL, "SELECT * FROM backpacks ORDER BY backpackID ASC", "Backpack_Load");
	return 1;
}