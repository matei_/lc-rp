/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

/* --- server dialogs --- */
public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	new bool: anuntat = false;
	if(!isnull(inputtext))
	{
		for(new _pos; inputtext[_pos] > 0; _pos ++)
		{
			if(inputtext[_pos] == '%')
			{
				inputtext[_pos] = '#';
				if(!anuntat)
				{
					GameTextForPlayer(playerid, "~r~CHARACTERS NOT ALLOWED", 3000, 4);
					anuntat = true;
				}
			}
		}
	}

	if(dialogid == DIALOG_INVALID || dialogid == DIALOG_UNUSED)
		return 1;

	switch(dialogid)
	{
		case DIALOG_LOGIN:
		{
			if(!response)
				return DelayedKick(playerid);

			if(strlen(inputtext) <= 8)
				return ShowPlayerDialog(playerid, DIALOG_LOGIN, DIALOG_STYLE_PASSWORD, "Master Account - Login (password too short)", "Your password must be longer than 8 characters!\nPlease retype the password in the box below:", "Login", "Abort");

			new hashed_pass[129];
			WP_Hash(hashed_pass, sizeof hashed_pass, inputtext);

			if(strcmp(hashed_pass, MasterInfo[playerid][masterPassword]) == 0)
			{
				MasterInfo[playerid][masterLogged] = true;
				GameTextForPlayer(playerid, "~y~ LOADING YOUR CHARACTERS", 10000, 4);
				T_LoadCharacters(playerid);
			}
			else
			{
				MasterInfo[playerid][masterLoginAttempts] ++;
				if(MasterInfo[playerid][masterLoginAttempts] == 3)
				{
					ShowPlayerDialog(playerid, DIALOG_UNUSED, DIALOG_STYLE_MSGBOX, "Master Account - Login (failed - wrong password)", "You have mistype your password too ofter (3 times)!\nYou have been kicked.", "Close", "");
					DelayedKick(playerid);
				}
				else
				{
					ShowPlayerDialog(playerid, DIALOG_LOGIN, DIALOG_STYLE_PASSWORD, "Master Account - Login (wrong password)", "Wrong password!\nPlease retype your password in the box below:", "Login", "Abort");
				}
			}
		}
		default:
			return 0;
	}
	return 1;
}