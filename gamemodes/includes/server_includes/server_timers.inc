/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

/* --- server timers --- */

DelayedKick(playerid, time = 500)
{
	SetTimerEx("_KickPlayerDelayed", time, false, "d", playerid);
	return 1;
}

T_LoadCharacters(playerid, time = 10000)
{
	SetTimerEx("QUERY_ListCharacters", time, false, "d", playerid);
	return 1;
}