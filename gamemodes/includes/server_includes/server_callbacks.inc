/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

/* --- server callbacks --- */
public OnVehicleSpawn(vehicleid)
{
	return 1;
}

public OnVehicleMod(playerid, vehicleid, componentid)
{
	return 1;
}

public OnVehicleStreamIn(vehicleid)
{
	return 1;
}

public OnObjectMoved(objectid)
{
	return 1;
}

public OnDynamicObjectMoved(objectid)
{
	return 1;
}

public OnPlayerUpdate(playerid)
{
	return 1;
}

public OnPlayerEditAttachedObject(playerid, response, index, modelid, boneid, Float:fOffsetX, Float:fOffsetY, Float:fOffsetZ, Float:fRotX, Float:fRotY, Float:fRotZ, Float:fScaleX, Float:fScaleY, Float:fScaleZ )
{
	return 1;
}

public OnPlayerInteriorChange(playerid, newinteriorid, oldinteriorid)
{
	return 1;
}

public OnEnterExitModShop(playerid, enterexit, interiorid)
{
	return 1;
}

public OnVehicleRespray(playerid, vehicleid, color1, color2)
{
	return 1;
}

public OnPlayerClickTextDraw(playerid, Text:clickedid)
{
	return 1;
}

public OnPlayerClickPlayerTextDraw(playerid, PlayerText:playertextid)
{
	return 1;
}

public OnPlayerClickMap(playerid, Float:fX, Float:fY, Float:fZ)
{
	return 1;
}

public OnPlayerStreamIn(playerid)
{
	return 1;
}

public OnPlayerStreamOut(playerid)
{
	return 1;
}

public OnPlayerConnect(playerid)
{
	g_MySQLRaceCheck[playerid] ++;

	for(new E_MASTER:M; M < E_MASTER; M ++)
		MasterInfo[playerid][M] = 0;

	new connectQuery[172];
	GetPlayerName(playerid, MasterInfo[playerid][masterName], MAX_PLAYER_NAME);

	SetPlayerCamera(playerid);
	InitFly(playerid);

	mysql_format(g_SQL, connectQuery, sizeof(connectQuery), "SELECT * FROM bannedlist WHERE IPAddress = '%e'", ReturnIP(playerid));
	mysql_tquery(g_SQL, connectQuery, "CheckBanList", "i", playerid);

	return 1;
}

public OnPlayerDisconnect(playerid, reason)
{
	g_MySQLRaceCheck[playerid] ++;
	UpdateCharacterPosition(playerid);
	return 1;
}

public OnRconLoginAttempt(ip[], password[], success)
{
	return 1;
}

public OnVehicleDeath(vehicleid, killerid)
{
	return 1;
}

public OnPlayerSpawn(playerid)
{
	PreloadAnimLib(playerid,"BOMBER");
    PreloadAnimLib(playerid,"RAPPING");
    PreloadAnimLib(playerid,"SHOP");
    PreloadAnimLib(playerid,"BEACH");
    PreloadAnimLib(playerid,"SMOKING");
    PreloadAnimLib(playerid,"FOOD");
    PreloadAnimLib(playerid,"ON_LOOKERS");
    PreloadAnimLib(playerid,"DEALER");
    PreloadAnimLib(playerid,"CRACK");
    PreloadAnimLib(playerid,"CARRY");
    PreloadAnimLib(playerid,"COP_AMBIENT");
    PreloadAnimLib(playerid,"PARK");
    PreloadAnimLib(playerid,"INT_HOUSE");
    PreloadAnimLib(playerid,"FOOD" );
	PreloadAnimLib(playerid,"PED" );
	PreloadAnimLib(playerid,"BAR" );
	PreloadAnimLib(playerid,"BASEBALL" );
	PreloadAnimLib(playerid,"CAMERA" );
	PreloadAnimLib(playerid,"CAR" );
	PreloadAnimLib(playerid,"CHAINSAW" );
	PreloadAnimLib(playerid,"DANCING" );
	PreloadAnimLib(playerid,"GANGS" );
	PreloadAnimLib(playerid,"GHANDS" );
	PreloadAnimLib(playerid,"GRAFFITI" );
	PreloadAnimLib(playerid,"SPRAYCAN" );
	PreloadAnimLib(playerid,"STRIP" );
	PreloadAnimLib(playerid,"SCRATCHING" );
	PreloadAnimLib(playerid,"MISC" );
	PreloadAnimLib(playerid,"SWEET" );
	PreloadAnimLib(playerid,"BSKTBALL" );
	PreloadAnimLib(playerid,"BOX" );
	PreloadAnimLib(playerid,"CAR_CHAT" );
	PreloadAnimLib(playerid,"RIOT" );

	if(FlyIsUsed[playerid] == true)
	{
		StopFly(playerid);
		FlyIsUsed[playerid] = false;
		SetPlayerHealth(playerid, 100);
	}
	return 1;
}

public OnPlayerLeaveCheckpoint(playerid)
{
	return 1;
}

public OnPlayerEnterCheckpoint(playerid)
{
	return 1;
}

public OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if(newkeys == KEY_SECONDARY_ATTACK)
	{
		if(FlyIsUsed[playerid] == true)
		{
			StopFly(playerid);
			FlyIsUsed[playerid] = false;
			SetPlayerHealth(playerid, 100);
			return 1;
		}
	}

	if(newkeys == KEY_SECONDARY_ATTACK)
	{
		ClearAnimations(playerid);
		return 1;
	}
	return 1;
}

public OnPlayerStateChange(playerid, newstate, oldstate)
{
	return 1;
}

public OnPlayerEnterVehicle(playerid, vehicleid, ispassenger)
{
	return 1;
}

public OnPlayerRequestClass(playerid, classid)
{
	TogglePlayerSpectating(playerid, true);
	return 1;
}

public OnPlayerCommandPerformed(playerid, cmd[], params[], result, flags)
{
	if(result == -1)
	{
		UnknownMessage(playerid, "This command does not exists.");
		return 0;
	}
	return 1;
}

public OnPlayerText(playerid, text[])
{
	if(CharacterInfo[playerid][characterLoggedIn] == false)
	{
		SendErrorMessage(playerid, "You must be logged in on your character to chat.");
	}
	else
	{
		SendNearbyMessage(playerid, 20.0, -1, "%s says: %s", ReturnName(playerid, 0), text);
	}
	return 0;
}

public OnUnoccupiedVehicleUpdate(vehicleid, playerid, passenger_seat, Float:new_x, Float:new_y, Float:new_z, Float:vel_x, Float:vel_y, Float:vel_z)
{
	return 1;
}

public OnPlayerClickPlayer(playerid, clickedplayerid, source)
{
	return 1;
}

public OnPlayerTakeDamage(playerid, issuerid, Float:amount, weaponid, bodypart)
{
	new Float: playerHP;
	GetPlayerHealth(playerid, playerHP);

	switch(weaponid)
	{
		case 4: SetPlayerHealth(playerid, playerHP - 30);
		case 22: SetPlayerHealth(playerid, playerHP - 25);
		case 32: SetPlayerHealth(playerid, playerHP - 25);
		case 28: SetPlayerHealth(playerid, playerHP - 25);
		case 30: SetPlayerHealth(playerid, playerHP - 30);
		case 31: SetPlayerHealth(playerid, playerHP - 21);
		case 34: SetPlayerHealth(playerid, playerHP - 70);
		case 29: SetPlayerHealth(playerid, playerHP - 25);
		case 25: SetPlayerHealth(playerid, playerHP - 40);
	}

	switch(bodypart)
	{
		case 9: SetPlayerHealth(playerid, 0);
	}

	if(!IsPlayerConnected(playerid)) return 0;
	if(!IsPlayerConnected(issuerid)) return 0;
	return 1;
}