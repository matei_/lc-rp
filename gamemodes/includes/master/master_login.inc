/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.

*/

/* --- master login --- */
function:CheckBanList(playerid)
{
	if(!cache_num_rows())
	{
		new existCheck[129];

		mysql_format(g_SQL, existCheck, sizeof(existCheck), "SELECT * FROM masteraccounts WHERE masterName = '%e' AND masterIsVerified = '1'", MasterInfo[playerid][masterName]);
		mysql_tquery(g_SQL, existCheck, "OnMasterDataLoaded", "dd", playerid, g_MySQLRaceCheck[playerid]);
	}
	else
	{
		new banRows, banFields;
		cache_get_data(banRows, banFields);

		new banDate[90], banAdmin[32], banReason[129];

		cache_get_field_content(0, "banDate", banDate, g_SQL, 90);
		cache_get_field_content(0, "bannedBy", banAdmin, g_SQL, 32);
		cache_get_field_content(0, "banReason", banReason, g_SQL, 129);

		SendServerMessage(playerid, "Your account {BB9955}\"%s\"{FFFFFF} (%s) is banned from our server.", ReturnName(playerid), ReturnIP(playerid));
		SendServerMessage(playerid, "You were banned by {FF6347}\"%s\"{FFFFFF} (on date: {FF6347}\"%s\"{FFFFFF}), reason: {FF6347}\"%s\"{FFFFFF}.", banAdmin, banDate, banReason);
		return DelayedKick(playerid);
	}
	return 1;
}

function:OnMasterDataLoaded(playerid, race_check)
{
	if(race_check != g_MySQLRaceCheck[playerid])
		return DelayedKick(playerid);

	new connectString[129];

	if(cache_num_rows() > 0)
	{
		AssignMasterData(playerid);
		format(connectString, sizeof(connectString), "This master account (%s) is verified. You can login by entering your password in the box down below:", MasterInfo[playerid][masterName]);
		ShowPlayerDialog(playerid, DIALOG_LOGIN, DIALOG_STYLE_PASSWORD, "Master Account - Login", connectString, "Login", "Abort");
		GameTextForPlayer(playerid, "~r~ACCOUNT VERIFIED!", 2500, 4);
		MasterInfo[playerid][masterRegistered] = true;
	}
	else
	{
		SendErrorMessage(playerid, "This master account (%s) is not verified or doesn't exists!", MasterInfo[playerid][masterName]);
		SendMessage(playerid, "To verify your account, check the email sent to you.");
		SendMessage(playerid, "from {BB9955}ucp@lc-roleplay.com{FFFFFF} and click the link that's inside the email.");
		SendMessage(playerid, "If you do not yet have an {BB9955}L.C. Roleplay{FFFFFF} account, you can");
		SendMessage(playerid, "create one on our {BB9955}User Control Panel (UCP){FFFFFF} at");
		SendMessage(playerid, "{BB9955}ucp.lc-roleplay.com{FFFFFF}.");
		GameTextForPlayer(playerid, "~r~ACCOUNT NOT VERIFIED!", 2500, 4);
		DelayedKick(playerid);
	}
	return 1;
}

function:AssignMasterData(playerid)
{
	MasterInfo[playerid][masterID] = cache_get_field_content_int(0, "masterID");
	MasterInfo[playerid][masterAdmin] = cache_get_field_content_int(0, "masterAdmin");
	MasterInfo[playerid][masterDonorLevel] = cache_get_field_content_int(0, "masterDonorLevel");
	MasterInfo[playerid][masterDonorDate] = cache_get_field_content_int(0, "masterDonorDate");

	//cache_get_field_content(0, "masterPassword", MasterInfo[playerid][masterPassword], g_SQL, 129);
	cache_get_field_content(0, "masterEmail", MasterInfo[playerid][masterEmail], g_SQL, 255);
	cache_get_field_content(0, "masterSecretQ", MasterInfo[playerid][masterSecretQ], g_SQL, 255);
	cache_get_field_content(0, "masterSecretA", MasterInfo[playerid][masterSecretA], g_SQL, 255);

	MasterInfo[playerid][masterIsDonor] = bool: cache_get_field_content_int(0, "masterIsDonor");
	MasterInfo[playerid][masterIsVerified] = bool: cache_get_field_content_int(0, "masterIsVerified");
	return 1;
}