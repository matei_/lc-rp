/*

This gamemode has been created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com.
All rights reserved by LC-ROLEPLAY.COM.
// FOR GIT

*/

#include <a_samp>

/* --- script version --- */

#define 	SERVER_VERSION		"LC-RP: v0.1"
#undef 		MAX_PLAYERS
#define 	MAX_PLAYERS 		500

#include 	<a_mysql>
#include 	<crashdetect>
#include 	<fly>
#include 	<foreach>
#include 	<sscanf2>
#include 	<streamer>
#include 	<Pawn.CMD>
#include 	<YSI\y_timers>
#include 	<YSI\y_utils>
#include 	<YSI\y_hooks>
#include 	<YSI\y_iterate>

/* --- server includes --- */
#include 	"..\includes\server_includes\server_defines.inc"
#include 	"..\includes\server_includes\server_enums.inc"
#include 	"..\includes\server_includes\server_variables.inc"
#include 	"..\includes\server_includes\server_timers.inc"
#include 	"..\includes\server_includes\server_functions.inc"
#include 	"..\includes\server_includes\server_mysql.inc"
#include 	"..\includes\server_includes\server_callbacks.inc"
#include 	"..\includes\server_includes\server_dialogs.inc"

/* --- master includes --- */
#include 	"..\includes\master\master_login.inc"

/* --- character includes --- */
#include 	"..\includes\character\character_loadandselect.inc"
#include 	"..\includes\character\character_functions.inc"

/* --- faction includes --- */
#include 	"..\includes\factions\faction_functions.inc"
#include 	"..\includes\factions\faction_commands.inc"

/* --- vehicle includes --- */
#include 	"..\includes\vehicles\vehicles_functions.inc"
#include 	"..\includes\vehicles\vehicles_commands.inc"

/* --- commands includes --- */
#include 	"..\includes\commands\admin_commands.inc"
#include 	"..\includes\commands\player_commands.inc"
#include 	"..\includes\commands\animlist.inc"

/* --- inventory includes --- */

main()
{
	print("gamemode created by Liberty City Roleplay Development Team (Webster & Tony) for lc-roleplay.com");
	printf("gamemode version: %s", SERVER_VERSION);
	return 1;
}

public OnGameModeInit()
{
	print("preparing the gamemode, please wait...");
	g_mysql_init();
	return 1;
}

public OnGameModeExit()
{
	print("exiting the gamemode, please wait...");
	g_mysql_exit();
	return 1;
}